package hu.bute.aait.gorog.tddtest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Gorog on 2014.09.21.
 */
@Config(emulateSdk = 18)
@RunWith(RobolectricTestRunner.class)
public class LoginActivityTest {
    private LoginActivity activity;

    @Before
    public void setup() {
        activity = Robolectric.buildActivity(LoginActivity.class).create().start().resume().get();
    }

    @Test
    public void shouldHaveApplicationName() throws Exception {
        String appName = activity.getResources().getString(R.string.app_name);
        assertNotEquals("", appName);
    }

    @Test
    public void shouldHaveLoginForm() throws Exception {
        assertNotNull(activity);

        assertNotNull(activity.userName);

        assertNotNull(activity.password);

        assertNotNull(activity.loginButton);
    }

    @Test
    public void performLogin() throws Exception {
        activity.userName.setText("user");
        activity.password.setText("password");

        activity.loginButton.performClick();

        assertEquals("user", LoginUtil.userName);
        assertEquals("password", LoginUtil.password);
    }
}